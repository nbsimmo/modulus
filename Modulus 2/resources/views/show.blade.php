@extends('layouts.master')
@section('title')
{{$handbooks->title}}
@stop
@section('content')
    <div id="content">
            <div id ="col1">
                <h1>{{$handbooks->title}}</h1>
                <img src="img/logo.png" alt="">
                <br><br>
                {{$default->text}}
                <br><br>

                <h2>Introduction</h2>
                <p>{{$handbooks->intro}}</p>

                <h2>Context</h2>
                <p>{{$handbooks->context}}</p>
                <h2>Coursework</h2>
                <p>{{$handbooks->context}}</p> 
                <h2>Schedule</h2>
                <p>{{$handbooks->intro}}</p>
               

            </div>
            <div id="col2">
                <h2>Inclusive Services</h2>
                {{$default->services}}
                <br><br>
                E-mail:
                {{$default->email}}
                <br><br>
                Tel:
                {{$default->tel}}
                <br><br>
                <h2>Tutor Contact Details</h2>
                <h3>{{$tutor->name}}</h3>
                <p>Should you require any assistance, feel free to contact using the below details.</p>
                E-mail:
                {{$tutor->email}}
                <br><br>
                Tel:
                {{$tutor->tel}}
                <br><br>

                <h2>Academic Conduct</h2>
                {{$default->conduct}}
                <br><br>
                
            
                
            </div>
    </div>
@stop
