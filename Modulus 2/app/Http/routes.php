<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Home and Create routes...
Route::get('home', 'HandbooksController@index');
Route::get('handbooks/create', 'HandbooksController@create');
Route::post('home', 'HandbooksController@store');

// Editor Routes
Route::get('editor', 'EditorController@index');
Route::post('editor/{id}', 'EditorController@update');

// Showing a specific handbook via ID
Route::get('{id}', 'HandbooksController@show');

// Schedule Editor Route
Route::get('schedule/editor', 'ScheduleController@index');

// Coursework Editor Route
Route::get('coursework/editor', 'CourseworkController@index');