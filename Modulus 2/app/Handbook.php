<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handbook extends Model
{
	protected $table = 'handbooks';
	protected $fillable = ['title', 'intro', 'context'];
	

}
