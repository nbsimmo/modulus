<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('users')->insert([
            'name' => 'Module Leader',
            'email' => 'module@leader.com',
            'password' => bcrypt('password'),
        ]);
    }
}
