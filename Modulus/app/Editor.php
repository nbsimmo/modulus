<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editor extends Model
{
    protected $table = 'resources';
	protected $fillable = ['text', 'conduct', 'services', 'email', 'tel'];
}
