@extends('layouts.master')
@section('title')
    Create New
@stop
    <div id="col1">
        <h1>Create a new Handbook!</h1>
        <p>Using the form to the right, input the data you would like to be present within the handbook</p>


    </div>

    <div id="col2">

    <form method="POST" action="{{ url('home') }}" enctype="multipart/form-data" id="create-hb" data-abide>

            <input type="text" id="title" name="title" placeholder="Handbook Title" value="{{ Input::old('title') }}" required>
            <br /><br />
            <textarea name="intro" placeholder="Handbook Introduction" id="intro" cols="10" rows="5" required></textarea>
            <br /><br />
            <textarea name="context" placeholder="Handbook Context" id="context" cols="10" rows="5" required></textarea>
            <br><br>
            <button id="submit" name="submit">Create Handbook</button>

        {!! csrf_field() !!}
    </form>
       
    </div>

