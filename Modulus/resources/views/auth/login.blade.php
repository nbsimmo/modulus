@extends('layouts.master')

@section('title')
	Login
@stop
    
<div class="cont">
	<div id="col1">
		<h1>Login</h1>
		<p>Use the form to the right to login!</p>
	</div>

	<div id="col2">

		<form method="POST" action="/auth/login">
	        {!! csrf_field() !!}
	            <input type="email" name="email" placeholder="E-mail" value="{{ old('email') }}">
				<br /><br />
	        	<input type="password" name="password" id="password" placeholder="Password">
				<br /><br />
	            <input type="checkbox" name="remember"> Remember Me
				<br /><br />
	            <button type="submit">Login</button>

    	</form>
		


	</div>
</div>