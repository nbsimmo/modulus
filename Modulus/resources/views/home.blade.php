@extends('layouts.master')
@section('title')
    Dashboard
@stop
@section('content')

<div id="content">
	<div id="col1">
		@foreach($handbooks as $handbook)
		<div>
			<h2><a href="{{$handbook->id}}">{{$handbook->title}}</a></h2>
		</div>
		@endforeach
	</div>
	<div id="col2">
		<a href="{{ url('handbooks/create') }}"><button>Create a New Handbook</button></a>
		<br><br>
		<a href="{{ url('editor') }}"><button>Resource Editor</button></a>
		<br><br>
		<a href="{{ url('schedule/editor') }}"><button>Handbook Schedules</button></a>
		<br><br>
		<a href="{{ url('coursework/editor') }}"><button>Coursework Editor</button></a>
	</div>
</div>
@stop
