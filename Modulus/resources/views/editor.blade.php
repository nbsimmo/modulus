@extends('layouts.master')
@section('title')
    Resource Editor
@stop
@section('content')

    <div id="col1">
        <h1>Shared Resource Editor</h1>
        <p>Using the form to the right, input the data you would like to be present within the handbook.</p>
        <hr>
        <h2>Current Information</h2>
        <h3>Cover Text:</h3>
        {{$info->text}}
        <h3>Academic Conduct:</h3>
        {{$info->conduct}}
        <h3>Inclusive Services:</h3>
        {{$info->services}}
        <h3>Inclusive E-mail Address:</h3>
        {{$info->email}}
        <h3>Inclusive Telephone Number:</h3>
        {{$info->tel}}
    </div>

    <div id="col2">

        <form method="POST" action="{{ url('editor') }}" enctype="multipart/form-data" id="create-hb" data-abide>

                <input type="text" id="text" name="text" placeholder="Handbook Cover Text" value="{{ Input::old('title') }}" required>
                <br /><br />
                <textarea name="conduct" placeholder="Academic Conduct" id="conduct" cols="10" rows="5" required></textarea>
                <br /><br />
                <textarea name="services" placeholder="Inclusive Services" id="services" cols="10" rows="5" required></textarea>
                <br><br>
                <input type="text" id="email" name="email" placeholder="E-mail Address" value="{{ Input::old('title') }}" required>
                <br /><br />
                <input type="text" id="tel" name="tel" placeholder="Telephone Number" value="{{ Input::old('title') }}" required>
                <br /><br />                
                <button id="submit" name="submit">Update!</button>

            {!! csrf_field() !!}
        </form>
       
    </div>
@stop
