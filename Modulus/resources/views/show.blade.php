@extends('layouts.master')
@section('title')
{{$handbooks->title}}
@stop
@section('content')
    <div id="content">
        <div id="hb-view">
            <div id ="hb">
                <h1>{{$handbooks->title}}</h1>
                {{$default->text}}
                <h2>Academic Conduct</h2>
                {{$default->conduct}}
                <h2>Inclusive Services</h2>
                {{$default->services}}
                <br><br>
                E-mail:
                {{$default->email}}
                <br><br>
                Tel:
                {{$default->tel}}


                <hr>
                <h2>Introduction</h2>
                <p>{{$handbooks->intro}}</p>
                <h2>Context</h2>
                <p>{{$handbooks->context}}</p>
                <hr>
                <h2>Schedule</h2>
                <p>{{$handbooks->intro}}</p>
                <h2>Coursework</h2>
                <p>{{$handbooks->context}}</p>
            </div>
        </div>
    </div>
@stop
