<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class module extends Model
{
    protected $table = 'modules';
    protected $fillable = ['id', 'handbook_id', 'title', 'code'];

    public function handbooks(){
    	return $this->hasMany('App\Handbook');
    }
}
