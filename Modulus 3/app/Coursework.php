<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coursework extends Model
{
    protected $table = 'coursework';
    protected $fillable = ['title', 'info'];
}
