<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handbook extends Model
{
	protected $table = 'handbooks';
	protected $fillable = ['title', 'intro', 'context', 'due', 'owner'];
	/*protected $dates = ['created_at', 'updated_at', 'due'];*/

	public function schedule(){
		return $this->hasMany('App\Schedule');
	}
	
	public function criteria(){
		return $this->hasMany('App\criteria');
	}

	public function module(){
		return $this->belongsTo('App\modules');
	}

	public function owner(){
		return $this->belongsTo('App\User', 'owner');
	}

}
