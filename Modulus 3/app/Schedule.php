<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule';
    protected $fillable = ['weekno', 'handbook_id', 'content', 'material'];

    public function handbooks(){
    	return $this->belongsTo('App\Handbook');
    }
}
