<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class criteria extends Model
{
    protected $table = 'criteria';
    protected $fillable = ['percentage', 'handbook_id', 'content'];

    public function handbooks(){
    	return $this->belongsTo('App\Handbook');
    }
}
