<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'UsersController@create');
Route::post('auth/register', 'UsersController@store');


// Edit Users
Route::get('users/show', 'UsersController@index');
Route::get('users/show/{id}', 'UsersController@edit');
Route::patch('users/show/{id}', 'UsersController@update');

// Schedule Editor Route
Route::get('{id}/schedule/create', 'HandbooksController@createItem');
Route::post('{id}/schedule', 'HandbooksController@storeItem');

// Criteria Editor Route
Route::get('{id}/criteria/create', 'HandbooksController@createCriteriaItem');
Route::post('{id}/criteria', 'HandbooksController@storeCriteriaItem');

// Home and Create routes...
Route::get('home', 'HandbooksController@index');
Route::get('handbooks/create', 'HandbooksController@create');
Route::post('home', 'HandbooksController@store');

// Editor Routes
Route::get('editor', 'EditorController@index');
Route::post('editor/{id}', 'EditorController@update');

// Showing a specific handbook via ID
Route::get('{id}', 'HandbooksController@show');
Route::post('{id}', 'HandbooksController@update');

// Coursework Editor Route
Route::get('coursework/editor', 'CourseworkController@index');

// Handbook deletion
Route::delete('{id}', 'HandbooksController@destroy');

// User deletion
Route::delete('users/{id}', 'UsersController@destroy');

