<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        $levels = [
            'Module Leader',
            'Admin',
            'Student'
        ];

        return view('users.show', ['users' => $users, 'levels' => $levels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    public function newUserstore(Request $request)
    {

        $this->validate($request, [
            'name'     => '',
            'email'    => '',
            'tel'    => '',
            'password' => bcrypt(''),
            
         ]);

        $user = User::create($request->all());

        return redirect('users');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User([

            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'tel' => $request->input('tel'),

        ]);

        // saves the item.
        $user->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $levels = [
            'Module Leader',
            'Admin',
            'Student'
        ];
        
        return view('users.edit', ['user' => $user, 'levels' => $levels]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::findOrFail($id);
        
        $user->update([

            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'tel'=>$request->input('tel'),
            'admin'=>$request->input('admin')



        ]);
        $user->save();
        return redirect('users/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('users/show');

    }
}
