<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Handbook;
use App\Editor;
use App\Schedule;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon;

class HandbooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $handbooks = Handbook::all();
        
        return view('home', ['handbooks' => $handbooks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('handbooks.create', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $handbook = new Handbook([

            'title' => $request->input('title'),
            'intro' => $request->input('intro'),
            'context' => $request->input('context'),
            'due' => $request->input('due'),
            'owner' => $request->input('owner')


        ]);

        // saves the item.
        $handbook->save();

        return redirect($handbook->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $handbooks = Handbook::with('owner')->findOrFail($id);
        $default = Editor::find(1);
        return view('show', ['handbooks' => $handbooks, 'default' => $default]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Handbook = Handbook::find($id);

        $Handbook->update([
            'intro' => $request->input('intro'),
            'context' => $request->input('context')


        ]);

        // saves the item.
        $Handbook->save();   

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $handbook = Handbook::findOrFail($id);
        
        $handbook->delete();

        return redirect('home');
    }

    public function createItem($id){
        $handbook = Handbook::findOrFail($id);
        return view('schedule.create', ['handbooks' => $handbook]);
    }

    public function storeItem(Request $request, $id){

        $this->validate($request, [

            'weekno' => 'required|unique:schedule,weekno,NULL,NULL,handbook_id,' . $id


        ]);

        $handbook = Handbook::findOrFail($id);

        $handbook->schedule()->create($request->all());

        return redirect('' . $id);
    }

    public function createCriteriaItem($id){
        $handbook = Handbook::findOrFail($id);
        return view('criteria.create', ['handbooks' => $handbook]);
    }

    public function storeCriteriaItem(Request $request, $id){

        $this->validate($request, [

            'percentage' => 'required|unique:criteria,percentage,NULL,NULL,handbook_id,' . $id


        ]);

        $handbook = Handbook::findOrFail($id);

        $handbook->criteria()->create($request->all());

        return redirect('' . $id);
    }
}
