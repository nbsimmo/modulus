@extends('layouts.master')

@section('title')
    Login
@stop

<div class="cont">
    <div id="col1">
        <h1>Register a new User</h1>
        <p>Use the form to the right to create a new user.</p>
        <p>Please speak to an administrator to grant admin permissions.</p>
    </div>

    <div id="col2">
<form method="POST" action="/auth/register">
    {!! csrf_field() !!}

    <div>
        <label for="name">Name</label>
        <input type="text" name="name" value="{{ old('name') }}">
    </div>

    <div>
        <label for="email">E-mail Address</label>
        <input type="email" name="email">
    </div>

    <div>
        <label for="password">Password</label>
        <input type="password" name="password">
    </div>

    <div>
        <label for="tel">Telephone Number</label>
        <input type="text" name="tel">
    </div>

    <div>
        <input type="submit" value="Register">
    </div>

</form>

</div>