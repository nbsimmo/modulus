@extends('layouts.master')

@section('title')
    Login
@stop
    
<div class="cont">
    <div id="col1">
        <h1>Login</h1>
        <p>Use the form to the right to login!</p>
    </div>

    <div id="col2">

        <form method="POST" action="/auth/login" class="login">
            {!! csrf_field() !!}
            <label for="email">E-mail Address</label>
                <input type="email" name="email" value="{{ old('email') }}">
                <br /><br />
                <label for="password">Password</label>
                <input type="password" name="password" id="password" placeholder="Password">
                <br /><br />
                <input type="checkbox" name="remember"> Remember Me
                <br /><br />
                <button type="submit">Login</button>

        </form>

        <a href="{{ url('auth\register') }}"><button>Register</button></a>
        


    </div>
</div>