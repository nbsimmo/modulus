<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="{{url('css/app.css')}}" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
<head>
    <title>Modulus - @yield('title')</title>
</head>
<body>
<div class="container">
    @include('/layouts/header')
    @yield('content')
    @include('/layouts/footer')
</div>
</body>
</html>
