<?php $admin = (Auth::user()->admin) ?>
@extends('layouts.master')
@section('title')
{{$handbooks->title}}
@stop
@section('content')
<div id="content">
    <div id="col3">
        <div id="green">
            <span>Edge Hill University</span>
            <hr>
        </div>

        <h2 id="ctext">{{$default->text}}<br>{{$handbooks->title}}</h2>
        <img src="img/logo.png" alt="">
        <div class="cont">
            <h2>Tutor Contact Details</h2>
            <h3>{{$handbooks->owner()->first()->name}}</h3>
            E-mail:
            {{$handbooks->owner()->first()->email}}
            <br><br>
            Tel:
            {{$handbooks->owner()->first()->tel}}
        </div>
        <div class="cont">
            <h2>Inclusive Services</h2>
            {{$default->services}}
            <p>Tel:</p>
            {{$default->tel}}
            <p>E-mail:</p>
            {{$default->email}}
        </div>
        <br>

        <div class="cont">
            <h2>Academic Conduct</h2>
            {{$default->conduct}}
        </div>
        <br>
        <div class="cont">
            <h2>Introduction</h2>
            <form method="POST" action="{{ url('' . $handbooks->id) }}" enctype="multipart/form-data" id="create-hb" data-abide>
                @if ($admin == '0'||$admin == '1')
                <textarea name="intro" id="intro" cols="10" rows="5" required>{{$handbooks->intro}}</textarea>

                @else
                <p class="cont-cont">{{$handbooks->intro}}</p>
                @endif
            </div>

            <div class="cont">
                <br>
                <h2>Coursework</h2>
                <form method="POST" action="{{ url('' . $handbooks->id) }}" enctype="multipart/form-data" id="create-hb" data-abide>
                    @if ($admin == '0'||$admin == '1')
                    <textarea name="context" id="context" cols="10" rows="5" required>{{$handbooks->context}}</textarea>
                    <button id="submit-btn" name="submit">Update</button>
                    {!! csrf_field() !!}
                </form>
                @else
                <p class="cont-cont">{{$handbooks->context}}</p>
                @endif         
            </div>
            <div class="cont">
                <br>
                <h2>Schedule</h2>
                <p id="caption">Below is the overall schedule that will take place during the course the module. A break down of the relavent information that will take place in each week and the relevent recommended reading material is also provided.</p>
                @if ($admin == '0'||$admin == '1')
                <p><a href="{{ url('' . $handbooks->id . '/schedule/create') }}"><button>New Item</button></a></p>
                <table>
                    <thead>
                        <tr>
                            <th>Week Number</th>
                            <th>Content</th>
                            <th>Reading Material</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($handbooks->schedule()->get() as $item)
                        <tr>
                            <td>{{$item->weekno}}</td>
                            <td>{{$item->content}}</td>
                            <td>{{$item->material}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <table>
                    <thead>
                        <tr>
                            <th>Week Number</th>
                            <th>Content</th>
                            <th>Reading Material</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($handbooks->schedule()->get() as $item)
                        <tr>
                            <td>{{$item->weekno}}</td>
                            <td>{{$item->content}}</td>
                            <td>{{$item->material}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

            </div> 
            <div class="cont">
                <br>
                <h2>Marking Scheme</h2>
                <p>Below is the marking scheme that has been set for the coursework assignments.</p>
                @if ($admin == '0'||$admin == '1')
                <p><a href="{{ url('' . $handbooks->id . '/criteria/create') }}"><button>New Item</button></a></p>
                <table>
                    <thead>
                        <tr>
                            <th>Percentage/Grade</th>
                            <th>Content</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($handbooks->criteria()->get() as $Criteriaitem)
                        <tr>
                            <td>{{$Criteriaitem->percentage}}&#37</td>
                            <td>{{$Criteriaitem->content}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <table>
                    <thead>
                        <tr>
                            <th>Percentage</th>
                            <th>Content</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($handbooks->criteria()->get() as $item)
                        <tr>
                            <td>{{$item->percentage}}&#37</td>
                            <td>{{$item->content}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>   

        </div>
        @stop
