<?php $admin = (Auth::user()->admin) ?>
@extends('layouts.master')
@section('title')
Users
@stop
@section('content')



<div id="content">

    <div id="col1">
        <h1>User List</h1>
        <p>The current list of users who are permitted to use the system are situated in the right-hand side section.<br>Use the buttons to the right of a user to perform a specific action.</p>
    </div>
    <div id="col2">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Telephone Number</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->tel}}</td>
                    <td>
                        {{ (($user->admin == '') ? 'None Set' : $levels[$user->admin]) }}
                    </td>
                    <td><a href="{{'show/' .$user->id}}"><button>Edit</button></a></td>
                    <td><form action="{{ url('users/' . $user->id) }}" method="POST">
                        <input name="_method" type="hidden" value="DELETE">
                        <button type="submit">Delete</button>
                        {!! csrf_field() !!}
                    </form></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
