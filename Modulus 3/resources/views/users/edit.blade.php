<?php $admin = (Auth::user()->admin) ?>
@extends('layouts.master')
@section('title')
Users
@stop
@section('content')

<div id="content">

    <div id="col1">
        <h1>Editing: {{$user->name}}</h1>
        <p>Using the form to the right, edit the details of {{$user->name}}</p>
    </div>
    <div id="col2">
        <form method="POST" action="{{ url('users/show/' . $user->id) }}">
            
            <input type="hidden" name="_method" value="PATCH">

            <p class="name-edit">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="{{ old('name', $user->name) }}">
            </p>
            
            <p>
                <label for="email-address">E-mail</label>
                <input type="email" name="email" id="email-address" value="{{$user->email}}">
            </p>

            <p>
                <label for="tel">Telephone Number</label>
                <input type="text" name="tel" id="tel" value="{{$user->tel}}">
            </p>

            <p>
                <label for="admin">Role</label>
                <select name="admin" id="admin">
                @foreach($levels as $key => $name)
                    <option value="{{ $key }}"  {!! (($user->admin == $key) ? 'selected' : '') !!}>{{ $name }}</option>
                @endforeach
                </select>
            </p>
            <input type="submit" value="Submit">

            {!! csrf_field() !!}
        </form>
    </div>
</div>
@stop
