@extends('layouts.master')
@section('title')
    Create New
@stop
    <div id="col1">
        <h1>Create a new Handbook!</h1>
        <p>Using the form to the right, input the data you would like to be present within the handbook</p>
        <p>Scheduling can be processed and entered later once the handbook has been created and clicking on the relative handbook.</p>
        
        

        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                
                </tr>
                @endforeach
            </tbody>
        </table>

        



    </div>

    <div id="col2">

    <form method="POST" action="{{ url('home') }}" enctype="multipart/form-data" id="create-hb" data-abide>

            <label for="title">Handbook Title</label>
            <input type="text" id="title" name="title" placeholder="Handbook Title" value="{{ Input::old('title') }}" required>
            <br /><br />
            <label for="intro">Handbook Introduction</label>
            <textarea name="intro" placeholder="Handbook Introduction" id="intro" cols="10" rows="5" required></textarea>
            <br /><br />
            <label for="context">Coursework</label>
            <textarea name="context" placeholder="Coursework" id="context" cols="10" rows="5" required></textarea>
            <br><br>
            <label for="owner">Handbook Owner ID</label>
            <input type="text" id="owner" name="owner" placeholder="Handbook Owner ID" value="{{ Input::old('title') }}" required>
            <br><br>
            <label for="due">Module Estimate Finish Date</label>
            <br>
            <input type="month" id="due" name="due" placeholder="Module Due" value="{{ Input::old('due') }}" required>
            <br><br>
            <button id="submit" name="submit">Create Handbook</button>

        {!! csrf_field() !!}
    </form>
       
    </div>

