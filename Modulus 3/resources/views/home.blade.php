<?php $admin = (Auth::user()->admin) ?>
@extends('layouts.master')
@section('title')
    Dashboard
@stop
@section('content')



<div id="content">

    <div id="col1">
    <h2>Currently Viewable Handbooks</h2><br>
        @foreach($handbooks as $handbook)
        <div>
            
            <a href="{{$handbook->id}}">{{$handbook->title}} - Module Finishes: {{$handbook->due}}</a>
            <form action="{{ url(''. $handbook->id) }}" method="POST">
                <input name="_method" type="hidden" value="DELETE">
                @if ($admin == '1' || $admin == '0')
                <button type="submit">Delete</button>
                @endif
                {!! csrf_field() !!}
            </form>
            <br>
        </div>
        @endforeach
    </div>
    <div id="col2">
    <h2>Functionality</h2><br>
    
    @if ($admin == '1')
        <a href="{{ url('handbooks/create') }}"><button>Create a New Handbook</button></a>
        <br><br>
        <a href="{{ url('editor') }}"><button>Shared Resource Text Editor</button></a>
        <br><br>
        <a href="{{ url('users/show') }}"><button>User List</button></a>
    @elseif ($admin == '0')
        <a href="{{ url('handbooks/create') }}"><button>Create a New Handbook</button></a>
    @else
    
    @endif
    </div>
</div>
@stop
