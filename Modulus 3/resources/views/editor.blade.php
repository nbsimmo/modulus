@extends('layouts.master')
@section('title')
    Resource Editor
@stop
@section('content')

    <div id="col1">
        <h1>Shared Resource Editor</h1>
        <p>Using the form to the right, input the data you would like to be present within the handbook.</p>

    </div>

    <div id="col2">

        <form method="POST" action="{{ url('editor/' . $info->id) }}" enctype="multipart/form-data" id="create-hb" data-abide>
                Handbook Cover Text:
                <input type="text" id="text" name="text" placeholder="Handbook Cover Text" value="{{$info->text}}" required>
                <br /><br />
                Academic Conduct:
                <textarea name="conduct" placeholder="Academic Conduct" id="conduct" cols="10" rows="5" required>{{$info->conduct}}</textarea>
                <br /><br />
                Inclusive Services:
                <textarea name="services" id="services" cols="10" rows="5" required>{{$info->services}}</textarea>
                <br><br>
                E-mail Address:
                <input type="text" id="email" name="email" placeholder="E-mail Address" value="{{$info->email}}" required>
                <br /><br />
                Telephone Number:
                <input type="text" id="tel" name="tel" placeholder="Telephone Number" value="{{$info->tel}}" required>
                <br /><br />                
                <button id="submit" name="submit">Update</button>

            {!! csrf_field() !!}
        </form>
       
    </div>
@stop
