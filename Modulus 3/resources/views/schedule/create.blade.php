@extends('layouts.master')
@section('title')
    Create Item
@stop
@section('content')

<div id="content">
    <div id="col1">
        <h1>Create a Schedule Item</h1>
        <p>Using the form to the right, add in the relative information you would like the schedule item to include.</p>
    </div>
    <div id="col2">
    @if(count($errors) > 0)
        <div>
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <form action="{{ url('' . $handbooks->id . '/schedule') }}" method="post">
            
            <label for="weekno">Week Number</label>
            <br>
            <input type="number" name="weekno" id="weekno" value="{{ old('weekno') }}">
            <br><br>
            <label for="content">Content</label>
            <br>
            <textarea name="content" id="content" cols="30" rows="10">{{ old('content') }}</textarea>
            <br><br>
            <label for="material">Material</label>
            <br>
            <textarea name="material" id="material" cols="30" rows="10">{{ old('material') }}</textarea>
            <input type="submit" value="Submit">

            {!! csrf_field() !!}
        </form>
    </div>
</div>


@stop