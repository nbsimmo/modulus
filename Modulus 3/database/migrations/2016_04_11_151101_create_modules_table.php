<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('handbook_id')->unsigned();
            $table->text('title');
            $table->text('code');
            $table->timestamps();

            $table->primary(['id', 'handbook_id']);
            $table->foreign('handbook_id')->references('id')->on('handbooks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
