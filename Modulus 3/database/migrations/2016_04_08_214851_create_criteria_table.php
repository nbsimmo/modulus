<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria', function (Blueprint $table) {
            $table->integer('percentage')->unsigned();
            $table->integer('handbook_id')->unsigned();
            $table->text('content');
            $table->timestamps();

            $table->primary(['percentage', 'handbook_id']);
            $table->foreign('handbook_id')->references('id')->on('handbooks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('criteria');
    }
}
